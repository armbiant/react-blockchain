import { Model3DCanvas, StarsCanvas } from './canvas';
import Hero from './Hero';
import Navbar from './Navbar';
import About from './About';

export {
  Hero,
  Navbar,
  About,
  Model3DCanvas, 
  StarsCanvas
}